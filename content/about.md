---
title: Well.. I'm not really a hobbit
draft: false
modified: 2021-05-30T20:47:06+02:00
---

Yeah, I'm not a hobbit.. but despite the fact I like my own comfort as hobbits do! My name is
Ondřej Míchal and I'm a computer science student at [FIT VUT Brno](https://www.fit.vut.cz). 
I work as an intern in the Desktop Team at [Red Hat](https://www.redhat.com) where I mainly co-maintain [Toolbox](https://github.com/containers/toolbox).

I'm a supporter of open source software and in my spare time I contribute to projects like [Fedora](https://getfedora.org) or [GNOME](https://www.gnome.org).

I love J.R.R.Tolkien.

On the internet you may know me under nicks "Marty", "MartyHarry" or "HarryMichal".
