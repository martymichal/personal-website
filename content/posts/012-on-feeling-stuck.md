---
title: "On feeling stuck"
date: 2024-10-21T00:30:00+02:00
tags:
  - university
  - diary
draft: false
---

For a bit over 2 years I've been feeling somewhat stuck in my life. Or rather I've lost the sense of direction I had in the past. The feeling is similar to the moment when you're walking somewhere only to realise that you no longer know for what purpose. Or if as somebody snapped the threads of a puppet. It's a feeling of being overwhelmed by everything which renders you incapable of acting.

Despite this feeling, my life is very good. I'm advancing in my studies, I'm being given great opportunities and I'm meeting great people from whom I've already learned a lot. I have discovered a community of people that inspire me to pay attention to my relationship with God. Life is being good to me. And I would like to snap out of this haze to be finally fully present. And it is proving to be the biggest challenge of my life.

I'm doing my best. It might not be enough but I'm trying my best. And that's fine. It is a start. That's one of the things I need to learn. To be kind to myself. Strive to be my best self, yes, but not through negativity. I also want to gain more humility. Most worthwhile goals cannot be achieved overnight. They need time, dedication, humility and a bit of luck. Humility is key because it makes us see when we need to learn, when to take a break and when to ask for help.

For a bit over 2 years I've been feeling somewhat stuck in my life. And yet, I need to acknowledge that I'm moving. More slowly and in different patterns but I'm moving. Maybe that's also a part of growing up.