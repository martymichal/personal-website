projpath=${1:-""}

if [ $EUID -ne 0 ]; then
  echo "Please run as root"
  exit 1
fi

if [[ -z "$projpath" ]]; then
  echo "Please specify absolute path to the project folder to the first argument"
  exit 2
fi

pushd "$projpath"

# Pull changes from remote (call with --force)
git pull --force --recurse-submodules

# Build the site
hugo

# Copy to /var/www/html/
cp -r ./public/* /var/www/html/

# Update ownership of all files/folders in /var/www/html
chown -R www-data /var/www/html/*

popd
